Upstream: https://gitlab.gnome.org/GNOME/tracker/-/merge_requests/603

From 45e070a5dc8080ef4a0521a17f3b8294f7bb1787 Mon Sep 17 00:00:00 2001
From: Calvin Walton <calvin.walton@kepstin.ca>
Date: Tue, 27 Jun 2023 20:50:35 -0400
Subject: [PATCH] build: Fix handling of datadir, etc. as absolute paths

There were several places in the meson files where path building was
done incorrectly, which causes breakage when some directories are passed
as absolute paths; for example on Exherbo we use
    -Dprefix=/usr/x86_64-pc-linux-gnu -Ddatadir=/usr/share
in our cross-prefix configuration.

There's two classes of errors that I've fixed here:

* Incorrect use of the / operator.
  The / operator is the correct operator to use when building paths,
  since it has special handling for absolute paths. If the right hand
  side of the operator is an absolute path, then it is returned; the
  left hand side is discarded. But there are some places in tracker
  where the left side of the / operator was not a path, for example in
  pkgconfig "variables", which resulted in an error from meson when the
  string no longer matched the right pattern. This is fixed by
  accounting for order of operations.

* Assuming that directory arguments are relative paths, and using them
  in string concatenation.
  The settings for LOCALEDIR and SHAREDIR were built by concatenating
  the prefix and specific dir variables with a / in between, which
  failed with absolute paths. Switch them to use the / operator, and the
  set_quoted function to avoid needing any string formatting.

While I was fixing these problems, I also noticed that the
tracker-testutils pc file was using absolute paths for the python_path
and command variables (rather than substituting in the ${prefix} or
${libdir} variables), which meant that it wasn't relocatable. I have
fixed that.
---
 meson.build                        |  4 ++--
 src/libtracker-sparql/meson.build  |  8 ++++----
 utils/trackertestutils/meson.build | 10 +++-------
 3 files changed, 9 insertions(+), 13 deletions(-)

diff --git a/meson.build b/meson.build
index 85ca880529..877179b243 100644
--- a/meson.build
+++ b/meson.build
@@ -330,8 +330,8 @@ conf.set('HAVE_LIBSTEMMER', have_libstemmer)
 
 conf.set('HAVE_STATVFS64', cc.has_header_symbol('sys/statvfs.h', 'statvfs64', args: '-D_LARGEFILE64_SOURCE'))
 
-conf.set('LOCALEDIR', '"@0@/@1@"'.format(get_option('prefix'), get_option('localedir')))
-conf.set('SHAREDIR', '"@0@/@1@"'.format(get_option('prefix'), get_option('datadir')))
+conf.set_quoted('LOCALEDIR', get_option('prefix') / get_option('localedir'))
+conf.set_quoted('SHAREDIR', get_option('prefix') / get_option('datadir'))
 
 conf.set('GETTEXT_PACKAGE', '"@0@"'.format(tracker_versioned_name))
 conf.set('PACKAGE_VERSION', '"@0@"'.format(meson.project_version()))
diff --git a/src/libtracker-sparql/meson.build b/src/libtracker-sparql/meson.build
index cc236806eb..1658e8185e 100644
--- a/src/libtracker-sparql/meson.build
+++ b/src/libtracker-sparql/meson.build
@@ -141,10 +141,10 @@ pkg.generate(libtracker_sparql,
     ],
     variables: [
         'exec_prefix=${prefix}',
-        'libexecdir=${prefix}' / get_option('libexecdir'),
-        'datadir=${prefix}' / get_option('datadir'),
-        'tracker_datadir=${datadir}' / tracker_versioned_name,
-        'ontologies_dir=' + tracker_ontologies_dir,
+        'libexecdir=' + '${prefix}' / get_option('libexecdir'),
+        'datadir=' + '${prefix}' / get_option('datadir'),
+        'tracker_datadir=${datadir}/' + tracker_versioned_name,
+        'ontologies_dir=${tracker_datadir}/ontologies',
     ],
 )
 
diff --git a/utils/trackertestutils/meson.build b/utils/trackertestutils/meson.build
index 091567d463..04b89bb778 100644
--- a/utils/trackertestutils/meson.build
+++ b/utils/trackertestutils/meson.build
@@ -11,11 +11,7 @@ sources = [
 ]
 
 if get_option('test_utils')
-  testutils_dir = get_option('test_utils_dir')
-
-  if testutils_dir == ''
-    testutils_dir = tracker_internal_libs_dir
-  endif
+  testutils_dir = tracker_internal_libs_dir / get_option('test_utils_dir')
 
   install_data(sources, install_dir: testutils_dir / 'trackertestutils')
 
@@ -33,8 +29,8 @@ if get_option('test_utils')
     name: 'tracker-testutils-' + tracker_api_version,
     description: 'tracker test utilities',
     variables: [
-      'python_path=' + testutils_dir,
-      'command=' + testutils_dir / 'trackertestutils' / 'tracker-sandbox',
+      'python_path=' + ( '${libdir}/tracker-' + tracker_api_version ) / get_option('test_utils_dir') / 'trackertestutils',
+      'command=${python_path}/tracker-sandbox',
     ]
   )
 
-- 
GitLab

