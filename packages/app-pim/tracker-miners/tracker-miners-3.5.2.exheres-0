# Copyright 2018 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Purpose License v2

require gnome.org [ suffix=tar.xz ]
require gsettings test-dbus-daemon
require meson [ cross_prefix=true ]
require option-renames [ renames=[ 'ms-office office' ] ]

SUMMARY="Collection of data extractors for Tracker"

LICENCES="GPL-2 LGPL-2.1"
SLOT="3.0"
PLATFORMS="~amd64 ~armv8 ~x86"

MYOPTIONS="
    cuesheet [[ description = [ enable external cuesheet parsing ] ]]
    exif [[ description = [ read EXIF photo metadata ] ]]
    gif [[ description = [ extract metadata from GIF images ] ]]
    gstreamer [[ description = [ use gstreamer for media extraction ] ]]
    iptc [[ description = [ read IPTC photo metadata ] ]]
    iso [[ description = [ extract metadata from ISO images ] ]]
    office [[ description = [ extract metadata from MS & Open Office documents ] ]]
    networkmanager [[ description = [ use NetworkManager to detect the current network status ] ]]
    pdf [[ description = [ extract metadata from PDF documents ] ]]
    playlist [[ description = [ extract metadata from playlists ] ]]
    raw [[ description = [ extract metadata from RAW photos ] ]]
    rss [[ description = [ index RSS feeds ] ]]
    systemd [[ description = [ install systemd user services ] ]]
    tiff [[ description = [ extract metadata from TIFF images ] ]]
    upower [[ description = [ battery/mains power detection ] ]]
    writeback [[
        description = [ save changes made in Tracker back to original file metadata ]
        requires = [ gstreamer ]
        note = [ GStreamer dep is automagic ]
    ]]
    xmp [[ description = [ read XMP photo metadata ] ]]
    xps [[ description = [ extract metadata from XPS documents ] ]]

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        app-doc/asciidoc
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        app-pim/tracker:3.0[>=3.5.0]
        dev-libs/glib:2[>=2.70.0]
        dev-libs/icu:=[>4.8.1.1]
        dev-libs/libxml2:2.0[>=2.6]
        gnome-desktop/gobject-introspection:1
        media-libs/libexif[>0.6]
        media-libs/libpng:=[>=1.2]
        sys-apps/dbus[>=1.3.1]
        sys-apps/util-linux
        sys-libs/libseccomp[>=2.0]
        cuesheet? ( media-libs/libcue[>=2.0.0] )
        exif? ( media-libs/libexif[>=0.6] )
        gif? ( media-libs/giflib:= )
        gstreamer? (
            media-libs/gstreamer:1.0[>=1.20]
            media-plugins/gst-plugins-base:1.0
        )
        iptc? ( media-libs/libiptcdata )
        iso? ( dev-libs/libosinfo:1.0[>=0.2.9] )
        office? ( office-libs/libgsf:1[>=1.14.24] )
        networkmanager? ( net-apps/NetworkManager )
        pdf? ( app-text/poppler[glib][>=0.16.0] )
        playlist? ( gnome-desktop/totem-pl-parser )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        raw? ( dev-libs/gexiv2 )
        rss? ( net-libs/libgrss[>=0.7] )
        systemd? ( sys-apps/systemd[>=242] )
        tiff? ( media-libs/tiff:= )
        upower? ( sys-apps/upower[>=0.9.0] )
        xmp? ( media-libs/exempi:2.0[>=2.1.0] )
        xps? ( dev-libs/libgxps )
    run:
        sys-apps/low-memory-monitor
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/455-build-Fix-handling-of-datadir-etc.-as-absolute-paths.patch
)

MESON_SRC_CONFIGURE_PARAMS=(
    -Dabiword=false

    -Dexif=enabled
    -Djpeg=enabled
    -Dpng=enabled
    -Dxml=enabled
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'cuesheet cue'
    'exif'
    'gif'
    'office gsf'
    'iptc'
    'iso'
    'networkmanager network_manager'
    'pdf'
    'playlist'
    'raw'
    'tiff'
    'xmp'
    'xps'
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'rss miner_rss'
    'systemd systemd_user_services'
    'writeback'
)

MESON_SRC_CONFIGURE_OPTIONS=(
    'gstreamer -Dgeneric_media_extractor=gstreamer -Dgeneric_media_extractor=none'
    'upower -Dbattery_detection=upower -Dbattery_detection=none'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dfunctional_tests=true -Dfunctional_tests=false'
)


# Tests that involve the crawler aren't actually finding/extracting the test files,
# and I haven't been able to figure out why that is.
RESTRICT="test"

src_test() {
    unset DISPLAY

    # redirect any configuration and user settings to temp via XDG variables
    export XDG_DATA_HOME=${TEMP}
    export XDG_CACHE_HOME=${TEMP}
    export XDG_CONFIG_HOME=${TEMP}

    # G_DBUS_COOKIE_SHA1_KEYRING_DIR requires 0700, ${TEMP} is 0755
    export G_DBUS_COOKIE_SHA1_KEYRING_DIR_IGNORE_PERMISSION=1
    export G_DBUS_COOKIE_SHA1_KEYRING_DIR=${TEMP}

    # use the memory backend for settings to ensure that the system settings in dconf remain
    # untouched by the tests
    export GSETTINGS_BACKEND=memory

    # the tracker-dbus/request test relies on g_debug() messages being output to stdout
    export G_MESSAGES_DEBUG=all

    # Unix sockets for temporary dbus created in the ${WORKBASE}/_build directory
    esandbox allow_net 'unix-abstract:./dbus-*'

    TZ=UTC LANG=C LC_ALL=C HOME="${TEMP}" test-dbus-daemon_run-tests meson_src_test

    esandbox disallow_net 'unix-abstract:./dbus-*'
}
