# Copyright 2008 Alexander Færøy <ahf@exherbo.org>
# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] python [ blacklist=2 multibuild=false ]
require meson

SUMMARY="GTK API documentation generator"
HOMEPAGE="https://wiki.gnome.org/DocumentationProject/GtkDoc"

UPSTEAM_DOCUMENTATION="
    https://developer-old.gnome.org/gtk-doc-manual/stable/ [[
        description = [ User manual ]
    ]]
"

LICENCES="FDL-1.1 GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="doc"

# known to be broken in 1.32
# - new mkhtml2.py fails tests, also imports lxml
# - new scan.py fails tests
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.19]
    build+run:
        app-text/docbook-xml-dtd:4.3
        app-text/docbook-xsl-stylesheets
        dev-libs/libxml2:2.0[>=2.3.6]
        dev-libs/libxslt
        dev-python/Pygments[python_abis:*(-)?]
        doc? ( gnome-desktop/yelp-tools )
    run:
        !app-doc/gtk-doc-autotools [[
            description = [ merged inside gtk-doc ]
            resolution = uninstall-blocked-before
        ]]
    test:
        dev-libs/glib:2[>=2.38.0]
        x11-libs/gtk+:3
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/ebf57ee71f9c6df2ab01a408590ccc4822547cea.patch
)

src_prepare() {
    meson_src_prepare

    edo sed \
        -e "/python.find_installation/ s/'python3'/'python$(python_get_abi)'/" \
        -i meson.build
}

MESON_SRC_CONFIGURE_PARAMS=(
    -Dautotools_support=true
    -Dcmake_support=true
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'doc yelp_manual'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

