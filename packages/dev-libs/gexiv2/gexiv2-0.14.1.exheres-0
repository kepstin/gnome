# Copyright 2010-2015 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require meson \
    python [ blacklist=2 multibuild=false with_opt=true ] \
    vala [ vala_dep=true with_opt=true ]

SUMMARY="GObject wrapper for Exiv2"
DESCRIPTION="
gexiv2 is a GObject-based wrapper around the Exiv2 library. It makes the basic features of Exiv2
available to GNOME applications.
"
HOMEPAGE="https://wiki.gnome.org/Projects/${PN}"
DOWNLOADS="mirror://gnome/sources/${PN}/$(ever range 1-2)/${PNV}.tar.xz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    tools

    ( python vapi ) [[ requires = [ gobject-introspection ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.6.4][python_abis:*(-)?] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.14] )
    build+run:
        dev-libs/glib:2[>=2.46.0]
        graphics/exiv2:=[>=0.26]
        python? ( gnome-bindings/pygobject:3 )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/06adc8fb70cb8c77c0cd364195d8251811106ef8.patch
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection introspection'
    'gtk-doc gtk_doc'
    'python python3'
    'tools'
    'vapi'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

MESON_SRC_CONFIGURE_TESTS=( "-Dtests=true -Dtests=false" )

src_install() {
    meson_src_install

    option python && python_bytecompile
}

