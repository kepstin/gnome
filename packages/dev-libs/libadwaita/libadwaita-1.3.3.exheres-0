# Copyright 2021 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require meson vala [ vala_dep=true with_opt=true ]

SUMMARY="Building blocks for modern GNOME applications"

LICENCES="LGPL-2.1"
SLOT="1"
PLATFORMS="~amd64"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    vapi [[ requires = [ gobject-introspection ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        gtk-doc? ( dev-doc/gi-docgen[>=2021.1] )
    build+run:
        dev-libs/fribidi
        dev-libs/glib:2[>=2.72.0]
        x11-libs/graphene:1.0
        x11-libs/gtk:4.0[>=4.9.5]
        x11-libs/pango[gobject-introspection?]
"

# requires X access
RESTRICT="test"

MESON_SOURCE="${WORKBASE}/${PNV/_/.}"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dexamples=false
    -Dprofiling=false
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'gobject-introspection introspection'
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc gtk_doc'
    vapi
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

