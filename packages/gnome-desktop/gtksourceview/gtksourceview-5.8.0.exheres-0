# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix="tar.xz" ]
require vala [ vala_dep=true with_opt=true ]
require meson
require gtk-icon-cache
require xdummy [ phase=test ]
require test-dbus-daemon

SUMMARY="Text widget with syntax highlighting support"
HOMEPAGE="https://wiki.gnome.org/Projects/GtkSourceView"

LICENCES="LGPL-2.1"
SLOT="5"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc [[ requires = [ gobject-introspection ] ]]
    vapi [[ requires = gobject-introspection ]]
"

DEPENDENCIES="
    build:
        dev-util/itstool
        sys-devel/gettext[>=0.19.4]
        virtual/pkg-config[>=0.20]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.70.0] )
        gtk-doc? ( dev-doc/gi-docgen )
    build+run:
        dev-libs/fribidi[>=0.19.7]
        dev-libs/glib:2[>=2.72]
        dev-libs/libxml2:2.0
        dev-libs/pcre2[>=10.21]
        media-libs/fontconfig
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk:4.0[>=4.6]
        x11-libs/pango
    test:
        x11-libs/gtk:4.0[X]
"

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'gobject-introspection introspection'
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc gtk_doc'
    'vapi'
)

src_prepare() {
    meson_src_prepare

    # 4.8.0: test-languagemanger has intermittent failures, possibly undefined behaviour?
    edo sed -e "/'test-languagemanager'/d" -i testsuite/meson.build
}

src_test() {
    xdummy_start
    declare -x GTK_A11Y=none

    nonfatal test-dbus-daemon_run-tests meson_src_test
    declare ret=$?

    xdummy_stop

    [[ ${ret} == 0 ]] || die 'Tests failed'
}
