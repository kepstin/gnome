# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require meson

SUMMARY="Playlist parsing library"
HOMEPAGE="http://library.gnome.org/devel/totem-pl-parser"

LICENCES="LGPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gtk-doc
    gobject-introspection
    libarchive [[ description = [ use libarchive to extract ISOs ] ]]
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.35.0]
        sys-devel/gettext
        virtual/pkg-config
        gtk-doc? ( dev-doc/gtk-doc[>=1.14] )
    build+run:
        dev-libs/glib:2[>=2.56.0]
        dev-libs/libgcrypt
        dev-libs/libxml2:2.0
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.6.14] )
        libarchive? ( app-arch/libarchive[>=3.0] )
"

MESON_SRC_CONFIGURE_PARAMS=(
    '-Denable-libgcrypt=yes'
    '-Denable-uchardet=no'
)
MESON_SRC_CONFIGURE_OPTION_ENABLES=(
    'gtk-doc'
    'libarchive libarchive yes no'
)

RESTRICT="test" # requires internet access

