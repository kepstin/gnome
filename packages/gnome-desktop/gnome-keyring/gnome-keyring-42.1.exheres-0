# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome-keyring

LICENCES="GPL-2"
SLOT="1"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="caps doc pam
    caps [[
        description = [ Enable Linux capabilities to prevent the keyring's memory from being paged to swap ]
    ]]
    pam [[ description = [ Install a PAM module to unlock the keyring on login ] ]]
    ssh-agent [[ description = [ Disable this to use the one from openssh or gnupg ] ]]
    systemd [[ description = [ Build with systemd socket activation support ] ]]

    ( linguas: af ar as ast az be be@latin bg bn bn_IN bs ca ca@valencia cs cy da de dz el en_CA
               en_GB en@shaw eo es et eu fa fi fr ga gl gu he hi hr hu id is it ja ka km kn ko lt
               lv mai mg mk ml mn mr ms nb ne nl nn oc or pa pl pt pt_BR ro ru rw si sk sl sq sr
               sr@latin sv ta te tg th tr ug uk vi xh zh_CN zh_HK zh_TW )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config[>=0.18]
        doc? ( dev-libs/libxslt )
    build+run:
        dev-libs/glib:2[>=2.44.0]
        dev-libs/libgcrypt[>=1.2.2]
        gnome-desktop/gcr:0[>=3.27.90][gobject-introspection]
        caps? ( sys-libs/libcap-ng )
        pam? ( sys-libs/pam )
        systemd? ( sys-apps/systemd )
"

# Requires X
RESTRICT="test"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-schemas-compile
    --disable-selinux
    --disable-p11-tests
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    doc
    pam
    ssh-agent
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'caps libcap-ng'
    "pam pam-dir /usr/$(exhost --target)/lib/security"
    systemd
)

