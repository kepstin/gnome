# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require gnome.org [ suffix=tar.xz ] gsettings gtk-icon-cache

SUMMARY="Window Manager for the GNOME environment"
HOMEPAGE="https://www.gnome.org/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="doc vulkan"

DEPENDENCIES="
    build:
        app-text/docbook-xml-dtd:4.5
        gnome-desktop/yelp-tools
        sys-devel/gettext[>=0.19.6]
        sys-devel/libtool
        virtual/pkg-config[>=0.20]
        doc? ( dev-util/itstool )
        vulkan? ( sys-libs/vulkan-headers )
    build+run:
        dev-libs/glib:2[>=2.67.3]
        gnome-desktop/gsettings-desktop-schemas[>=3.3.0]
        gnome-desktop/zenity
        media-libs/libcanberra[providers:gtk3]
        x11-libs/gtk+:3[>=3.24.6]
        x11-libs/pango[>=1.2.0][X(+)]
        x11-libs/libXcomposite[>=0.3]
        x11-libs/libXinerama
        x11-libs/libXpresent
        x11-libs/libXres
        x11-libs/startup-notification[>=0.7]
        vulkan? ( sys-libs/vulkan-loader )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--enable-canberra'
    '--enable-startup-notification'
    '--enable-xinerama'
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'doc themes-documentation'
    vulkan
)

pkg_postinst() {
    gtk_icon_cache_exlib_update_theme_cache
    gsettings_exlib_compile_gsettings_schemas
}

pkg_postrm() {
    gtk_icon_cache_exlib_update_theme_cache
    gsettings_exlib_compile_gsettings_schemas
}

